require 'sinatra'

get '/' do
  send_file 'public/index.html', type: 'text/html; charset=utf-8'
end
