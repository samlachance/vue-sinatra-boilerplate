import Vue from 'vue'
import Router from 'vue-router'
import Foo from './views/Foo.vue'
import Bar from './views/Bar.vue'
import Baz from './views/Baz.vue'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', component: Foo },
    { path: '/bar', component: Bar },
    { path: '/baz/:qux', component: Baz }
  ]
})
