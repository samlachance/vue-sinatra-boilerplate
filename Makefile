prod: build sinatra

setup:
	bundle install
	npm install

webpack:
	npm run dev

sinatra:
	rackup

build:
	npm run build
	